> МазЪтЪ e Организации с нестопанска цел.
 Стартита есентта на 2009г някъде в северните квартали на София.

..  image:: img/studio1-logo.png
   :height: 351px
   :width: 300px
   :scale: 50%
   :alt: ENS logo
   :align: right

.. toctree::
  :maxdepth: 1
  :caption: ОБОРУДВАНЕ

  equipment/computer-systems
  equipment/mixers-tracks
  equipment/sound-systems

.. toctree::
  :maxdepth: 1
  :caption: ИНСТРУМЕНТИ

  instruments/keyboards
  instruments/strings
  instruments/drums
  instruments/brass
  instruments/analog
  instruments/midi


.. toctree::
  :maxdepth: 1
  :caption: АПАРАТНА

  room/env
  room/workstation
  room/racks


  ЗВУКОЗАПИС
  ИНЖИНЕРИ
  ЦЕНИ
  РЪКОВОДСТВА
